insert into templo values (1, 'Shaolin Rivas', 'Rivas');
insert into templo values (2, 'Shaolin Ventilla', 'Madrid');

insert into profesor values (1, 'Carlos', 'Shifu');
insert into profesor values (2, 'David', 'Shi-Hing');
insert into profesor values (3, 'Rober', 'Lao-Shi');

insert into alumno values (1, 'Marta', 'Blanco', '2020-01-01');
insert into alumno values (2, 'Adrian', null, '2014-09-01');
insert into alumno values (3, 'Antonio', 'Azul-verde', '2018-10-05');

insert into tipo_clase values (1, 'Kung-Fu', 25);
insert into tipo_clase values (2, 'Sanda', 30);
insert into tipo_clase values (3, 'Chi-kun', 20);

insert into clase values (1, 
	(select id from templo where ubicacion = 'Rivas'),
	(select id from profesor where nombre_profesor = 'Rober'), 
    (select id from alumno where nombre_alumno = 'Adrian'),
    (select id from tipo_clase where nombre_clase = 'Sanda'));
insert into clase values (2, 
	(select id from templo where ubicacion = 'Rivas'),
	(select id from profesor where nombre_profesor = 'Rober'), 
    (select id from alumno where nombre_alumno = 'Adrian'),
    (select id from tipo_clase where nombre_clase = 'Kung-Fu'));
insert into clase values (3, 
	(select id from templo where ubicacion = 'Madrid'),
	(select id from profesor where nombre_profesor = 'David'), 
    (select id from alumno where nombre_alumno = 'Marta'),
    (select id from tipo_clase where nombre_clase = 'Sanda'));
insert into clase values (4, 
	(select id from templo where ubicacion = 'Madrid'),
	(select id from profesor where nombre_profesor = 'David'), 
    (select id from alumno where nombre_alumno = 'Marta'),
    (select id from tipo_clase where nombre_clase = 'Kung-Fu'));
insert into clase values (5, 
	(select id from templo where ubicacion = 'Rivas'),
	(select id from profesor where nombre_profesor = 'Rober'), 
    (select id from alumno where nombre_alumno = 'Antonio'),
    (select id from tipo_clase where nombre_clase = 'Sanda'));
insert into clase values (6, 
	(select id from templo where ubicacion = 'Rivas'),
	(select id from profesor where nombre_profesor = 'Rober'), 
    (select id from alumno where nombre_alumno = 'Antonio'),
    (select id from tipo_clase where nombre_clase = 'Kung-Fu'));
insert into clase values (7, 
	(select id from templo where ubicacion = 'Rivas'),
	(select id from profesor where nombre_profesor = 'Carlos'), 
    (select id from alumno where nombre_alumno = 'Antonio'),
    (select id from tipo_clase where nombre_clase = 'Chi-kun'));


select * from templo;
select * from profesor;
select * from alumno;
select * from tipo_clase;
select * from clase;

-- Buscar clases a las que se ha apuntado Marta
select nombre_alumno, nombre_clase from clase
	inner join alumno on clase.id_alumno = alumno.id
    inner join tipo_clase on clase.id_tipo_clase = tipo_clase.id
where nombre_alumno = 'Antonio';

-- Lista de alumnos apuntados a Sanda
select nombre_alumno, nombre_clase, nombre_profesor from clase
	inner join alumno on clase.id_alumno = alumno.id
    inner join tipo_clase on clase.id_tipo_clase = tipo_clase.id
    inner join profesor on clase.id_profesor = profesor.id
where nombre_clase = 'Sanda';

-- Borrar las clases donde esta el alumno de id 3.
DELETE FROM clase where id_alumno = 3; 

-- Borrar al alumno con id 3. En la tabla alumno no podemos borrar a Antionio antes que de las clases porque es FK.
DELETE FROM alumno WHERE id = 3;

-- Cambiar fecha de inicio de alumno 1.
UPDATE alumno set fecha_inicio = '2020-01-02' where id = 1;
