CREATE TABLE templo (
id INT NOT NULL,
nombre_templo VARCHAR (25),
ubicacion VARCHAR (50),
PRIMARY KEY (id)
);

CREATE TABLE profesor (
id INT,
nombre_profesor VARCHAR (25),
rango VARCHAR (25),
PRIMARY KEY (id)
);

CREATE TABLE alumno (
id INT NOT NULL,
nombre_alumno VARCHAR (25),
rango VARCHAR (25),
fecha_inicio DATE,
PRIMARY KEY (id)
);

CREATE TABLE tipo_clase (
id INT NOT NULL,
nombre_clase VARCHAR (25),
precio INT,
PRIMARY KEY (id)
);

CREATE TABLE clase (
id INT NOT NULL,
id_tipo_clase INT,
id_templo INT,
id_profesor INT,
id_alumno INT,
PRIMARY KEY (id)
);

ALTER TABLE clase ADD foreign key (id_templo) references templo (id);
ALTER TABLE clase ADD foreign key (id_profe) references profesor (id);
ALTER TABLE clase ADD foreign key (id_alumno) references alumno (id);
ALTER TABLE clase ADD foreign key (id_tipo_clase) references tipo_clase (id);